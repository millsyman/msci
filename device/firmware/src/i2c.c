
#include "system_config/Hardware/framework/peripheral/i2c/plib_i2c.h"
#include "system_config/Hardware/framework/system/console/sys_console.h"
bool transmit_message(uint8_t address, uint8_t * message, int length) {
    if (PLIB_I2C_BusIsIdle(I2C_ID_2)) {
        PLIB_I2C_MasterStart(I2C_ID_2);
        while(!PLIB_I2C_BusIsIdle(I2C_ID_2));
        PLIB_I2C_TransmitterByteSend(I2C_ID_2, address);
        long int i;
        for (i = 0; i <= length; i++) {
            while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_ID_2));
            PLIB_I2C_TransmitterByteSend(I2C_ID_2, *(message+i));
        }
        return true;
    } else {
        return false;
    }
}