import numpy as np
import unittest
import main


class StarTest(unittest.TestCase):
    def test_star_straight(self):
        star = main.Star(np.random.rand()*np.pi)
        altitude = np.random.rand()*2
        speed = np.random.rand()*30
        star.execute(altitude, speed, 0, 0)
        self.assertAlmostEqual(speed, star.backwards(altitude)[0][1])

    def test_star_angle(self):
        star = main.Star(np.random.rand()*45)
        altitude = np.random.rand() * 2
        speed = np.random.rand() * 30
        theta = np.random.rand() * 45
        phi = np.random.rand() * 45
        star.execute(altitude, speed, theta, phi)
        v_x = speed * np.sin(np.deg2rad(theta)) * np.cos(np.deg2rad(phi))
        v_y = speed * np.cos(np.deg2rad(theta)) * np.cos(np.deg2rad(phi))
        v_z = speed * np.cos(np.deg2rad(theta)) * np.sin(np.deg2rad(phi))
        v_out = star.backwards(altitude)[0]
        self.assertAlmostEqual(v_x, v_out[0])
        self.assertAlmostEqual(v_y, v_out[1])
        self.assertAlmostEqual(v_z, v_out[2])


class PyramidTest(unittest.TestCase):
    def test_pyramid_straight(self):
        pyramid = main.Pyramid(np.random.rand()*np.pi)
        altitude = np.random.rand()*2
        speed = np.random.rand()*30
        pyramid.execute(altitude, speed, 0, 0)
        self.assertAlmostEqual(speed, pyramid.backwards(altitude)[0][1])

    def test_pyramid_angle(self):
        pyramid = main.Pyramid(np.random.rand()*45)
        altitude = np.random.rand() * 2
        speed = np.random.rand() * 30
        theta = np.random.rand() * 45
        phi = np.random.rand() * 45
        pyramid.execute(altitude, speed, theta, phi)
        v_x = speed * np.sin(np.deg2rad(theta)) * np.cos(np.deg2rad(phi))
        v_y = speed * np.cos(np.deg2rad(theta)) * np.cos(np.deg2rad(phi))
        v_z = speed * np.cos(np.deg2rad(theta)) * np.sin(np.deg2rad(phi))
        v_out = pyramid.backwards(altitude)[0]
        self.assertAlmostEqual(v_x, v_out[0])
        self.assertAlmostEqual(v_y, v_out[1])
        self.assertAlmostEqual(v_z, v_out[2])


class SquarePyramidTest(unittest.TestCase):
    def test_square_pyramid_straight(self):
        square_pyramid = main.SquarePyramid(np.random.rand()*np.pi)
        altitude = np.random.rand()*2
        speed = np.random.rand()*30
        square_pyramid.execute(altitude, speed, 0, 0)
        self.assertAlmostEqual(speed, square_pyramid.backwards(altitude)[0][1])

    def test_square_pyramid_angle(self):
        square_pyramid = main.SquarePyramid(np.random.rand()*45)
        altitude = np.random.rand() * 2
        speed = np.random.rand() * 30
        theta = np.random.rand() * 45
        phi = np.random.rand() * 45
        square_pyramid.execute(altitude, speed, theta, phi)
        v_x = speed * np.sin(np.deg2rad(theta)) * np.cos(np.deg2rad(phi))
        v_y = speed * np.cos(np.deg2rad(theta)) * np.cos(np.deg2rad(phi))
        v_z = speed * np.cos(np.deg2rad(theta)) * np.sin(np.deg2rad(phi))
        v_out = square_pyramid.backwards(altitude)[0]
        self.assertAlmostEqual(v_x, v_out[0])
        self.assertAlmostEqual(v_y, v_out[1])
        self.assertAlmostEqual(v_z, v_out[2])
