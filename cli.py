import main as mn


def cli():

    configuration = str(input(
        "Which configuration of sensors would you like to test? (Star, Pyramid, SquarePyramid) "))
    # Not sure how to describe that one...
    sensor_angle = float(input("Sensor angle (defined in degrees of tilt) "))

    if (configuration == 'star') or (configuration == 'Star'):
        device = mn.Star(sensor_angle)

    elif (configuration == 'pyramid') or (configuration == 'Pyramid'):
        device = mn.Pyramid(sensor_angle)

    elif (configuration == 'squarepyramid') or (configuration == 'SquarePyramid'):
        device = mn.SquarePyramid(sensor_angle)

    else:
        pass

    altitude = float(input("Altitude (in km) "))
    airspeed = float(input("Air velocity vector (in m/s) "))
    theta = float(input("Theta (in degrees) "))
    phi = float(input("Phi (in degrees) "))
    do_what = str(input(
        "Calculate pressure on each sensor (p), air velocity vector (v), or both (pv)? "))

    device.execute(altitude, airspeed, theta, phi)

    if do_what == 'p':
        print_pressure(device)

    elif do_what == 'v':
        print_velocity(device, altitude)

    elif do_what == 'pv':
        print_pressure(device)
        print_velocity(device, altitude)


def print_velocity(device, altitude):
    out_velocity = device.backwards(altitude)
    print("Air velocity vector (m/s): ")
    print("    x: {:.3f}".format(out_velocity[0][0]))
    print("    y: {:.3f}".format(out_velocity[0][1]))
    print("    z: {:.3f}".format(out_velocity[0][2]))


def print_pressure(device):
    print("Pressures on sensors (Pa):")
    for n, pressure in enumerate(device.pressures):
        print("    {}: {:.3f}".format(n, pressure))


cli()
