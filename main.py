import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from abc import ABC
from typing import Tuple
from scipy.stats.mstats import gmean

import isosa


def static_pressure(altitude: float) -> float:
    return isosa.Atmosphere(altitude)[1] * isosa.PZERO


def density(altitude: float) -> float:
    return isosa.Atmosphere(altitude)[0] * isosa.RHOZERO


def single_plane(altitude: float, speed: float) -> float:
    static = static_pressure(altitude)
    ram = density(altitude) * speed**2
    return static + ram


def angle_plane(altitude: float, speed: float, theta: float, phi: float, normal: np.array) -> float:
    theta_rads = np.deg2rad(theta)
    phi_rads = np.deg2rad(phi)
    static = static_pressure(altitude)
    incident_air_dir = np.array([
        np.sin(theta_rads)*np.cos(phi_rads),
        np.cos(theta_rads)*np.cos(phi_rads),
        np.cos(theta_rads)*np.sin(phi_rads)
    ])
    ram = density(altitude) * speed**2 * np.dot(incident_air_dir, normal)**2
    return static + ram


class BaseDevice(ABC):
    def execute(self, altitude: float, speed: float, theta: float, phi: float) -> None:
        self.pressures = np.array(list(
            map(
                lambda x: angle_plane(altitude, speed, theta, phi, x),
                self.normals
            )
        ))

    def backwards(self, altitude: float) -> Tuple[np.array, np.array, np.array]:
        s_pressure = static_pressure(altitude)
        ram = self.pressures - s_pressure
        coeffs_squared = ram/density(altitude)
        coeffs = np.sqrt(coeffs_squared)
        velocity_vector = np.linalg.lstsq(self.normals, coeffs)
        return velocity_vector


class Star(BaseDevice):
    def __init__(self, angle: float) -> None:
        self.angle = np.deg2rad(angle)
        n1 = np.array([0, np.cos(self.angle), -np.sin(self.angle)])
        n2 = np.array([-np.sin(self.angle), np.cos(self.angle), 0])
        n3 = np.array([0, np.cos(self.angle), np.sin(self.angle)])
        n4 = np.array([np.sin(self.angle), np.cos(self.angle), 0])
        self.normals = np.array([n1, n2, n3, n4])

    def print(self, output=0):
        if output == 0:
            output = self.pressures
        print("         {:8.3f}         ".format(output[0]))
        print("{:8.3f}         {:8.3f}".format(output[1], output[2]))
        print("         {:8.3f}         ".format(output[3]))


class SquarePyramid(BaseDevice):
    def __init__(self, angle: float) -> None:
        self.angle = np.deg2rad(angle)
        n1 = np.array([0, 1, 0])
        n2 = np.array([0, np.cos(self.angle), -np.sin(self.angle)])
        n3 = np.array([-np.sin(self.angle), np.cos(self.angle), 0])
        n4 = np.array([0, np.cos(self.angle), np.sin(self.angle)])
        n5 = np.array([np.sin(self.angle), np.cos(self.angle), 0])
        self.normals = np.array([n1, n2, n3, n4, n5])

    def print(self, output=0):
        if output == 0:
            output = self.pressures
        print("             {:8.3f}             ".format(output[1]))
        print("{:8.3f}        {:8.3f}         {:8.3f}".format(
            output[4], output[0], output[2]))
        print("             {:8.3f}             ".format(output[3]))


class Pyramid(BaseDevice):
    def __init__(self, angle: float) -> None:
        self.angle = np.deg2rad(angle)
        n1 = np.array([0, 1, 0])
        n2 = np.array([
            0,
            np.cos(self.angle),
            -np.sin(self.angle)
        ])
        cos30 = np.cos(np.deg2rad(30))
        cos60 = np.cos(np.deg2rad(60))
        n3 = np.array([
            -cos60*np.sin(self.angle),
            np.cos(self.angle),
            cos30*np.sin(self.angle)
        ])
        n4 = np.array([
            cos60*np.sin(self.angle),
            np.cos(self.angle),
            cos30*np.sin(self.angle)
        ])
        self.normals = np.array([n1, n2, n3, n4])

    def print(self, output=0):
        if output == 0:
            output = self.pressures
        print("          {:8.3f}          ".format(output[1]))
        print("          {:8.3f}          ".format(output[0]))
        print("  {:8.3f}           {:8.3f}  ".format(output[3], output[2]))


# Test it out
if __name__ == "__main__":
    star = SquarePyramid(15.)

    star.execute(1, 25, 0, 10)

    print(star.pressures)


def sensor_diffs(p: np.array):
    matrix = np.array([p, ] * len(p))
    transpose = matrix.transpose()
    return np.sum(np.abs(matrix-transpose))


def fitness(p1: np.array, p2: np.array):
    p1_diff = sensor_diffs(p1)
    p2_diff = sensor_diffs(p2)
    angle_diff = np.mean(abs(p1-p2))
    return gmean([p1_diff, p2_diff, angle_diff])


def tables():
    config = [Star, Pyramid, SquarePyramid]
    col_names = ["Star", "Pyramid", "SquarePyramid"]            
    angle = np.arange(2, 90, 2)
    table = pd.DataFrame(index=angle, columns=col_names)
    a1 = 0.1
    a2 = 10

    for j, shape in enumerate(config):
        
        array1 = [shape(deg) for deg in angle]
        [element.execute(1, 25, 0, a1) for element in array1]
        p1 = [a.pressures for a in array1]

        array2 = [shape(deg) for deg in angle]
        [element.execute(1, 25, 0, a2) for element in array2]
        p2 = [a.pressures for a in array2]

        table[col_names[j]] = [fitness(p1[i], p2[i]) for i in np.arange(len(p1))]

        # a = config(15)
    
        # a1 = 5
        # a.execute(1, 25, 0, a1)
        # p1 = a.pressures

        # a2 = 10
        # a.execute(1, 25, 0, a2)
        # p2 = a.pressures

    print(table)


def make_fit(device: BaseDevice, a1=0, a2=10, altitude=1, speed=25, theta=0):
    device.execute(altitude, speed, theta, a1)
    p1 = device.pressures
    device.execute(altitude, speed, theta, a2)
    p2 = device.pressures
    return fitness(p1, p2)


def a(device):
    devices = np.array([device(angle) for angle in np.arange(0, 20, 0.5)])
    q = [make_fit(dev) for dev in devices]

    plt.plot(np.arange(0, 20, 0.5), q)

def do_a():
    a(Star)
    a(Pyramid)
    a(SquarePyramid)
    plt.xlabel("Angle")
    plt.ylabel("Fitness")
    plt.legend()
    plt.show()
